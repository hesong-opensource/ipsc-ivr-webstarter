# IPSC IVR Web Starter

该程序通过提供 Web API 启动 IPSC CTI Service 的 IVR

## Web API

### 启动 IVR 流程

- URL: `/{server_unit_id}/{server_process_index}/{project_id}/{flow_id}/startup`

  - URL 路径中的变量部分

    | Name                   | Description                                                  |
    | ---------------------- | ------------------------------------------------------------ |
    | `server_unit_id`       | 向 `IPSC` 数据总线上的 `ID` 是这个值的服务程序发送命令       |
    | `server_process_index` | 向 `IPSC` 数据总线上的子进程 `ID` 是这个值的服务程序发送命令 |
    | `project_id`           | 要调用的 `IVR` 流程所属的`IPSC`流程项目的 `ID`               |
    | `flow_id`              | 启动该项目中 `ID` 是这个值的流程                             |

- Method::

  POST

- Request Content:

  将被原样传入到 `IVR`

  - `Content-Type`: `application/json`

- Response Status:

  - `200`: 流程启动成功
  - `500`: 流程启动出现错误，或本服务出现错误
  - `504`: 流程启动超时未回复

- Response Content:

  - `Content-Type`: `application/json`
  - `JSON` 对象属性:

    如果 IVR 启动成功，返回 HTTP Status 200 OK，其内容 JSON 对象属性有：

    | Attribute | Data Type | Description                            |        |
    | --------- | --------- | -------------------------------------- | ------ |
    | `fiid`    | String    | 被启动的流程实例的 ID                  | 必须   |
    | `data`    | Any       | 流程启动返回的结果数据（不是流程结束） | 非必须 |

    eg:

    ```js
    {
      "fiid": "f0WauSnjgosjhgdAsdg",
      "data": <Any>
    }
    ```

    如果 IVR 启动失败，返回 HTTP Status 500 Internal Error，其内容 JSON 对象属性有：

    | Attribute | Data Type | Description |        |
    | --------- | --------- | ----------- | ------ |
    | `code`    | Integer   | 错误编码    | 必须   |
    | `message` | String    | 错误消息    | 非必须 |
    | `data`    | Any       | 错误数据    | 非必须 |

    eg:

    ```json
    {
      "code": 0,
      "message": ""
    }
    ```
