# Changelog

## 0.2

current state: rc3

- Change
  - 增加 IPSC Data Bus 的通信错误处理
  - 调整 View 错误日志，现在应该使用 aiohttp.server 这个 Logger 记录！
  - 去掉不必要的全局变量模组
  - 默认日志目录修改为 `logs`
- Optimize
  - 大量修复 lint 工具提示的问题
  - 优化文档记录
  - 优化 `pip` 与 `pipenv` 配置文件

## 0.1.2

Date: 2018-07-17

- Fix
  - 当启动IVR异常的时候，返回错误信息的参数类型错误。

- Optimize
  - 启动流程错误的日志记录和错误返回

- Docs: 增加文档内容
