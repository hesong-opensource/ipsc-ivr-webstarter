# -*- coding: utf-8 -*-

"""
程序包的版本信息
"""

__all__ = ['__version__', 'version_info']

from ._version import version as __version__
from ._version import version_tuple as version_info
