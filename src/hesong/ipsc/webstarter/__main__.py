#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import asyncio
import logging
import logging.config
import sys
from concurrent.futures import ThreadPoolExecutor
from pathlib import PurePosixPath
from typing import Any, Dict

import yaml
from pkg_resources import resource_string

from . import version
from .http.client import HttpClient
from .http.server import HttpServer
from .ipsc.controller import Controller as IpscController

try:
    import uvloop
except ImportError:
    uvloop = None
else:
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

PACKAGE_NAMESPACE = '.'.join(version.__name__.split('.')[:-1])  # 本包名称空间


def get_argument_parser() -> argparse.ArgumentParser:
    """
    设置并处理命令行参数
    """
    parser = argparse.ArgumentParser(
        prog=version.__package__,
        description='该程序通过提供 Web API 启动 IPSC CTI Service 的 IVR'
    )
    parser.add_argument('--version', action='version',
                        version='%(prog)s {0}'.format(version.__version__))

    subparsers = parser.add_subparsers(dest='sub_command', description='',
                                       help='<SubCommand --help> 输出帮助信息')

    parser_run = subparsers.add_parser('run', help='运行这个服务程序')
    group_http = parser_run.add_argument_group('HTTP', '该程序内嵌的 HTTP Server 选项')
    group_http.add_argument('-a', '--http-ip',
                            type=str, required=False,
                            help='该服务程序的 HTTP 监听地址 。如指定，将覆盖配置文件中的设置。')
    group_http.add_argument('-p', '--http-port',
                            type=int, required=False,
                            help='该服务程序的 HTTP 监听端口。如指定，将覆盖配置文件中的设置。')
    group_ipsc = parser_run.add_argument_group(
        'IPSC', '该程序的 IPSC Data Bus Network Client 选项')
    group_ipsc.add_argument('-u', '--ipsc-unit',
                            type=int, required=False,
                            help='该服务程序连接的 IPSC 数据总线所使用的本地客户端单元 ID。'
                                 '此 ID 必须大于等于16，小于256，且在局域网中不得重复。如指定，将覆盖配置文件中的设置。')
    group_ipsc.add_argument('-i', '--ipsc-ip',
                            type=str, required=False,
                            help='该服务程序要连接的 IPSC 数据总线服务的 IP 地址。如指定，将覆盖配置文件中的设置。')
    group_ipsc.add_argument('-o', '--ipsc-port',
                            type=int, required=False,
                            help='该服务程序要连接的 IPSC 数据总线服务的端口。如指定，将覆盖配置文件中的设置。')
    group_ipsc.add_argument('-n', '--ipsc-index',
                            type=int, required=False,
                            help='该服务程序所对应的 IPSC 服务程序的进程序号。如指定，将覆盖配置文件中的设置。')
    group_ivr = parser_run.add_argument_group('IVR 参数', 'IVR 项目相关参数')
    group_ivr.add_argument('-t', '--ivr-project',
                           type=str, required=False,
                           help='要使用的 IVR 项目 ID。如指定，将覆盖配置文件中的设置。')
    group_conf = parser_run.add_argument_group(
        'Configuration File', '该程序的配置文件选项')
    group_conf.add_argument('-c', '--program-config-file',
                            type=str, required=False,
                            help='程序配置文件')
    group_conf.add_argument('-l', '--logging-config-file',
                            type=str, required=False,
                            help='日志配置文件')
    parser_echo_config_sample = subparsers.add_parser(
        'echo_config_sample', help='输出配置文件样例')
    parser_echo_config_sample.add_argument('config', type=str, choices=['prog', 'log'],
                                           help='要输出配置文件样例')

    return parser


def load_config(args: argparse.Namespace) -> Dict[str, Any]:  # pylint:disable=R0912
    """从配置文件和命令行参数加载应用程序设置

    :return: 配置 dict (复杂的多层次内容，见配置 yaml 样例)
    :rtype: dict

    命令行参数将覆盖配置文件
    """
    logger = get_logger()
    if args.program_config_file:
        logger.info('加载配置文件 %s', args.program_config_file)
        with open(args.program_config_file, encoding='utf8') as fp:
            config = yaml.safe_load(fp)
    else:
        logger.info('未指定配置文件')
        config = {}

    # ThreadPoolExecutor config
    if 'ThreadPoolExecutor' in config:
        cfg = config['ThreadPoolExecutor']
        logger.debug('设置默认 ThreadPoolExecutor: %r', cfg)
        executor = ThreadPoolExecutor(**cfg)
        asyncio.get_event_loop().set_default_executor(executor)

    # config for http
    if 'http' not in config:
        config['http'] = {}
    cfg = config['http']
    if args.http_ip is not None:
        cfg['ip'] = args.http_ip
    if args.http_port is not None:
        cfg['port'] = args.http_port

    # config for ipsc
    if 'ipsc' not in config:
        config['ipsc'] = {}
    cfg = config['ipsc']
    if args.ipsc_ip is not None:
        cfg['ip'] = args.ipsc_ip
    if args.ipsc_port is not None:
        cfg['port'] = args.ipsc_port
    if args.ipsc_index is not None:
        cfg['index'] = args.ipsc_index
    if args.ipsc_unit is not None:
        cfg['unit'] = args.ipsc_unit

    # Debug config
    logger.info('config: %r', config)

    return config


def get_logger() -> logging.Logger:
    """Get the logger for the module

    :rtype: logging.Logger
    """
    return logging.getLogger('.'.join([PACKAGE_NAMESPACE, 'main']))


def set_log(args: argparse.Namespace):
    """初始化 logging 系统
    """
    if args.logging_config_file:
        with open(args.logging_config_file, encoding='utf8') as fp:
            cfg = yaml.safe_load(fp)
            logging.config.dictConfig(cfg)
            logging.info('已加载日志配置文件 %s', args.logging_config_file)
    else:
        logging.basicConfig(
            stream=sys.stdout,
            level=logging.INFO,
            format='%(asctime)-15s [%(threadName)-10s] %(levelname).1s [%(name)s] - %(message)s'
        )
        logging.warning('未指定日志配置文件，采用默认日志配置')


def main():
    """
    主函数
    """
    parser = get_argument_parser()
    args = parser.parse_args()

    if args.sub_command == 'echo_config_sample':
        path = PurePosixPath('data', 'config-samples',
                             '{0}.yml'.format(args.config))
        cfg_txt = resource_string(
            version.__package__, str(path)).decode('utf8')
        print()
        print(cfg_txt.strip())
        print()

    elif args.sub_command == 'run':
        # config: logging
        set_log(args)
        logger = get_logger()

        # 程序启动日志
        logger.info('#' * 60)
        logger.info('# 程序启动')
        logger.info('#' * 60)

        # 加载配置
        config = load_config(args)

        # Ipsc Controller
        cfg = config['ipsc']
        logger.info('初始化 IPSC Controller: %s', cfg)
        IpscController.initial(**cfg)
        logger.info('启动 IPSC Controller')
        IpscController.startup()

        # HTTP Client
        logger.info('初始化 HttpClient')
        HttpClient.initial()

        # HttpServer
        cfg = config['http']
        logger.info('启动 HttpServer: %s', cfg)
        HttpServer.start(**cfg)

        logger.info('启动程序: Ok')
        loop = asyncio.get_event_loop()
        try:
            loop.run_forever()
        except KeyboardInterrupt:
            logger.warning('结束程序: SIGINT')
        finally:
            HttpServer.stop()
            logger.info('结束程序: 关闭 EventLoop')
            loop.close()
            logger.info('结束程序: 关闭 IPSC Controller')
            IpscController.dispose()
            logger.info('#' * 60)
            logger.info('# 程序退出')
            logger.info('#' * 60)

    else:
        parser.print_help()


if __name__ == '__main__':
    main()
