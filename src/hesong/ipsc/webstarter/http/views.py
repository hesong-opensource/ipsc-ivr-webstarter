# -*- coding: utf-8 -*-

"""
views module
"""

import asyncio
from typing import Any, Dict, Optional, Text, Union

from aiohttp import web
from hesong.ipsc.busnetcli.errors import SmartBusError
from hesong.utils.loggermixin import LoggerMixin

from ..ipsc.controller import Controller
from ..ipsc.error import IpscError

try:
    import ujson as json
except ImportError:
    import json


class FlowStartUp(web.View, LoggerMixin):
    async def post(self):
        server_unit_id = int(self.request.match_info['server_unit_id'])
        server_process_index = int(
            self.request.match_info['server_process_index'])
        project_id = self.request.match_info['project_id']  # type: str
        flow_id = self.request.match_info['flow_id']  # type: str
        timeout = self.request.query.get('timeout')
        if timeout is not None:
            timeout = float(timeout)
        try:
            data = await self.request.json(loads=json.loads)
        except ValueError as err:
            return web.HTTPNotAcceptable(text='Invalid JSON content: {0}'.format(err))
        self.logger.debug('%s: [%s]:\n[%r]',
                          self.request.url, hex(id(self)), data)
        result = await self.start_ivr_flow(server_unit_id, server_process_index, project_id, flow_id, data, timeout)
        if isinstance(result, web.HTTPException):
            return result
        return web.json_response(result)

    async def start_ivr_flow(
            self, server_unit_id: int, server_process_index: int, project_id: str, flow_id: str,
            params: Optional[Dict[Text, Any]] = None, timeout: Optional[Union[int, float]] = None
    ):  # pylint:disable=too-many-arguments
        self.logger.debug(
            '%s: [%s] 启动流程 [%s.%s]<%s::%s>: 开始',
            self.request.url, hex(id(self)), server_unit_id, server_process_index, project_id, flow_id)
        try:
            result = await Controller.run_flow(
                server_unit_id, server_process_index, project_id, flow_id, params, timeout
            )
        except asyncio.TimeoutError:
            self.logger.error(
                '%s: [%s] 启动流程 <%s::%s>: 超时',
                self.request.url, hex(id(self)), project_id, flow_id)
            return web.HTTPGatewayTimeout()
        except SmartBusError as err:
            self.logger.error(
                '%s: [%s] 启动流程 <%s::%s>: 通信错误: %s',
                self.request.url, hex(id(self)), project_id, flow_id, err)
            return web.HTTPInternalServerError(text='{}'.format(err))
        except IpscError as err:
            self.logger.error(
                '%s: [%s] 启动流程 <%s::%s>: 失败: %s',
                self.request.url, hex(id(self)), project_id, flow_id, err)
            return web.HTTPInternalServerError(body=json.dumps(err.jsonify()), content_type='application/json')
        except Exception as err:  # pylint: disable=broad-except
            self.logger.exception('%s: [%s] 启动流程 <%s::%s>: 异常', self.request.url, hex(
                id(self)), project_id, flow_id)
            return web.HTTPInternalServerError(text='{}'.format(err))
        else:
            self.logger.debug(
                '%s: [%s] 启动流程 <%s::%s>: 成功',
                self.request.url, hex(id(self)), project_id, flow_id)
            return result
