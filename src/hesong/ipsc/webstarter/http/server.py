# -*- coding: utf-8 -*-

"""
Http Server module
"""

import asyncio

from aiohttp import web
from hesong.utils.loggermixin import LoggerMixin

from . import views

__all__ = ['HttpServer']

ROUTES = [
    web.post(
        '/{server_unit_id}/{server_process_index}/{project_id}/{flow_id}/startup', views.FlowStartUp),
]


class HttpServer(LoggerMixin):
    """
    Http Server
    """
    loop = None
    runner = None

    @classmethod
    def start(cls, ip, port, loop=None):
        """
        Start HTTP Server

        :param str ip:
        :param int port:
        :param asyncio.AbstractEventLoop|None loop:

        .. note:: The method running in blocking mode
        """
        logger = cls.get_logger()
        logger.info('start: ip=%s, port=%s', ip, port)
        if loop is None:
            loop = asyncio.get_event_loop()
        cls.loop = loop

        app = web.Application(loop=loop)
        app.add_routes(ROUTES)

        cls.runner = web.AppRunner(app)
        cls.loop.run_until_complete(cls.runner.setup())
        site = web.TCPSite(cls.runner, ip, port)
        cls.loop.run_until_complete(site.start())
        logger.info('start: listen on %s', cls.runner.addresses)

    @classmethod
    def stop(cls):
        """
        Stop HTTP Server

        .. note:: The method running in blocking mode
        """
        cls.get_logger().info('stop')
        assert cls.loop is not None
        assert cls.runner is not None
        cls.loop.run_until_complete(cls.runner.shutdown())
