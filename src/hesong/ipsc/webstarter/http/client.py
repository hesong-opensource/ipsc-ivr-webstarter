# -*- coding: utf-8 -*-

"""
HTTP 客户端
"""

import asyncio

import aiohttp

try:
    import ujson as json
except ImportError:
    import json

__all__ = ['HttpClient']


class HttpClient:
    """
    HTTP 客户端
    """
    _client_session = None

    @classmethod
    def initial(cls, loop=None):
        """
        :param asyncio.AbstractEventLoop loop:
        """
        if loop is None:
            loop = asyncio.get_event_loop()

        def _initial():
            cls._client_session = aiohttp.ClientSession(
                json_serialize=json.dumps, raise_for_status=True)

        loop.call_soon_threadsafe(_initial)

    @classmethod
    def make_request(cls):
        """返回一个 AsyncIO HTTP 客户端

        :return: HTTP 客户端
        :rtype: aiohttp.ClientSession
        """
        return cls._client_session
