# -*- coding: utf-8 -*-

"""
RPC 本地实现的容器
"""

from inspect import isclass, iscoroutinefunction

from .dictcontainer import BaseDictContainer

__all__ = ['RpcContainer']


class BaseRpcContainer(BaseDictContainer):
    """
    RPC 容器基础类

    单独继承一个基础类，避免 static container 成员被重复使用
    """


class RpcContainer(metaclass=BaseRpcContainer):
    """
    RPC 容器
    """

    @classmethod
    def decorate(cls, method):
        """
        :param str|None method:
        """

        def _wrapper(obj):
            cls.add_method(method, obj)
            return obj

        return _wrapper

    @classmethod
    def add_method(cls, method, obj):
        """
        :param str|None method:
        :param callable|coroutinefunction obj:
        """
        if not isinstance(method, str):
            raise TypeError('Argument "method" MUST be str')
        if method in cls:
            raise KeyError(
                'Duplicated definition in method dict: "{}"'.format(method))
        cls.chk_obj(obj)
        cls[method] = obj

    @classmethod
    def chk_obj(cls, obj):
        """
        :param callable|coroutinefunction obj:
        """
        if isclass(obj):
            execute = getattr(obj, 'execute')
            if execute is None:
                raise TypeError('{} has no "execute" method'.format(obj))
        else:
            execute = obj
        if iscoroutinefunction(execute) and not callable(execute):
            raise TypeError('Can not execute {!r} as a RPC'.format(obj))

    @classmethod
    def make_decorator(cls, prefix='', delimiter='.'):
        """
        :param str prefix:
        :param str delimiter:
        :rtype: Callable
        """
        prefix = prefix.strip().strip(delimiter)

        def _decorator(name=None):
            _name = name

            def _wrapper(obj):
                nonlocal _name
                if not _name:
                    _name = obj.__name__
                if prefix:
                    full_method_name = delimiter.join([prefix, _name])
                else:
                    full_method_name = _name
                cls.add_method(full_method_name, obj)
                return obj

            return _wrapper

        return _decorator
