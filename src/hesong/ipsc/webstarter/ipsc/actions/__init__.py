# -*- coding: utf-8 -*-

"""
RPC 的实现放在这里

.. important:: 在这里绑定所有RPC子模块，否则RPC会找不到方法！
"""

__all__ = ['bind_all']


def bind_all():
    """
    载入所有的RPC实现的定义
    """
    from . import misc  # pylint:disable=unused-variable,unused-import
