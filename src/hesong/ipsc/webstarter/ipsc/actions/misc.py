# -*- coding: utf-8 -*-

"""
测试性质的远程方法的实现
"""

import asyncio

from ..rpccontainer import RpcContainer

rpc = RpcContainer.make_decorator('app.misc')  # pylint: disable=C0103


@rpc()
def echo(msg):
    """原样返回收到的消息

    :param msg: 消息
    :type msg: str

    :return: 原样返回 ``msg``
    :rtype: str
    """
    return msg


@rpc()
async def delay(seconds, msg=''):
    """Echo after sleep

    :param float seconds:
    :param str msg:

    :return: 原样返回 ``msg``
    :rtype: str
    """
    await asyncio.sleep(float(seconds))
    return msg
