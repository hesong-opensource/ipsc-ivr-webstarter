# -*- coding: utf-8 -*-

"""
Dictionary 容器
"""

__all__ = ['BaseDictContainer', 'create_dict_container']


class BaseDictContainer(type):
    """
    A dictionary style container's basic meta class
    """

    def __new__(mcs, name, bases, atts):  # pylint:disable=bad-mcs-classmethod-argument
        mcs._items = {}
        return super().__new__(mcs, name, bases, atts)

    def __setitem__(self, key, value):  # pylint: disable=C0203
        self._items[key] = value

    def __delitem__(self, key):  # pylint: disable=C0203
        del self._items[key]

    def __getitem__(self, key):  # pylint: disable=C0203
        return self._items[key]

    def __contains__(self, key):  # pylint: disable=C0203
        return key in self._items

    def __iter__(self):  # pylint: disable=C0203
        yield from self._items.values()

    def __len__(self):  # pylint: disable=C0203
        return len(self._items)


def create_dict_container(key_validator=None, value_validator=None, raise_duplicated_key=False):
    """新建并返回一个 dict 容器类型

    :param callable key_validator: Key 验证函数， ``None`` 表示不进行验证。
        如果验证通过，必须返回 ``True`` ，如果验证不通过，应返回 ``False`` 或者抛出异常。

        函数的形式是::

            def validate(key):
                if ok:
                    return True
                else:
                    return False

    :param callable value_validator: Value 验证函数， ``None`` 表示不进行验证。
        如果验证通过，必须返回 ``True`` ，如果验证不通过，应返回 ``False`` 或者抛出异常。
        这个函数的形式与 ``key_validator`` 相同。

    :param raise_duplicated_key: dict 容器是否在 key 重复时抛出异常。
    :return: 新建的 dict 容器类型
    :rtype: BaseDictContainer
    """

    def _chk_key(key):
        if key_validator is not None:
            if not key_validator(key):
                raise KeyError('{}'.format(key))

    class _Container(BaseDictContainer):
        def __setitem__(self, key, value):  # pylint: disable=C0203
            _chk_key(key)
            if raise_duplicated_key:
                if key in self._items:
                    raise KeyError('{}'.format(key))
            if value_validator is not None:
                if not value_validator(value):
                    raise ValueError('{}'.format(value))
            super().__setitem__(key, value)

        def __delitem__(self, key):  # pylint: disable=C0203
            _chk_key(key)
            super().__delitem__(key)

        def __getitem__(self, key):  # pylint: disable=C0203
            _chk_key(key)
            return super().__getitem__(key)

        def __contains__(self, key):  # pylint: disable=C0203
            _chk_key(key)
            return super().__contains__(key)

        def __iter__(self):  # pylint: disable=C0203
            yield from super().__iter__()

        def __len__(self):  # pylint: disable=C0203,W0235
            return super().__len__()

    return _Container
