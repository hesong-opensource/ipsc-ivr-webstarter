# -*- coding: utf-8 -*-

"""
IPSC controller
"""

import asyncio
import datetime
import inspect
import logging
from functools import partial
from time import time
from typing import Any, Dict, List, Optional, Text, Union
from uuid import uuid1

from hesong.ipsc.busnetcli import Client
from hesong.utils import jsonrpc
from hesong.utils.jsonutils import json_dumps
from hesong.utils.loggermixin import LoggerMixin
from hesong.utils.stringhelper import to_str

from . import actions, error
from .rpccontainer import RpcContainer

CLIENT_ID = 1
CLIENT_TYPE = 12


class Controller(LoggerMixin):
    """
    IPSC 命令控制器
    """

    DEFAULT_FLOW_TIMEOUT = 5
    DEFAULT_FLOW_EXPIRES = 5

    @classmethod
    def initial(
            cls, unit: int, ip: str, port: int, index: int, loop: Optional[asyncio.AbstractEventLoop] = None
    ):  # pylint:disable=invalid-name,too-many-arguments
        """初始化库
        """
        logger = cls.get_logger()
        logger.info('bind actions')
        actions.bind_all()
        logger.info('initial(unit=%s, ip=%s, port=%s, index=%s)',
                    unit, ip, port, index)
        if loop is None:
            loop = asyncio.get_event_loop()
        cls._loop = loop
        cls._index = int(index)
        Client.initialize(unit)
        cls._client = Client.create(CLIENT_ID, CLIENT_TYPE, ip, port)
        cls._client.on_data = cls._client_on_data
        cls._client.on_flow_resp = cls._client_on_flow_resp
        cls._client.on_flow_timeout = cls._client_on_flow_timeout
        cls._client.on_flow_error = cls._client_on_flow_error

    @classmethod
    def dispose(cls):
        """
        释放库
        """
        Client.release()

    @classmethod
    def startup(cls):
        """
        控制器启动
        """
        cls._client.activate()

    _loop = None
    _client = None
    _index = None
    _call_future_map = {}

    @classmethod
    def get_client(cls):
        """
        返回控制器的 :class:`Client` 实例
        """
        return cls._client

    @classmethod
    async def _handle(cls, head, msg):
        # pylint:disable=too-many-locals,too-many-statements,too-many-branches,too-many-nested-blocks,too-many-statements
        logger = cls.get_logger()
        begin_time = time()
        parsed = None
        try:
            # pylint: disable=W0703
            # noinspection PyBroadException
            try:
                parsed = jsonrpc.parse(msg, True)
            except jsonrpc.Error as err:
                log_txt = 'RPC head={} msg={}. Parse error: {!r}'.format(
                    head, msg, err)
                if logger.getEffectiveLevel() > logging.DEBUG:
                    logger.error(log_txt)
                else:
                    logger.exception(log_txt)
                if err.id is not None:  # responsible JSON-RPC parse errors.
                    cls.send_response(
                        head.src_unit_id, head.src_unit_client_id, err.id, err.to_json())
            except Exception as err:
                log_txt = 'RPC head={} msg={}. Parse error: {!r}'.format(
                    head, msg, err)
                if logger.getEffectiveLevel() > logging.DEBUG:
                    logger.error(log_txt)
                else:
                    logger.exception(log_txt)
            # JSON-RPC Request
            if isinstance(parsed, jsonrpc.Request):
                # 转为 HTTP Request 交给 plum-ws 处理!
                logger.debug('>>> request[%s] %s(*args=%s, **kwargs=%s)',
                             parsed.id, parsed.method, parsed.args, parsed.kwargs)
                try:
                    result = None
                    if parsed.method == '_ivr.start_success':
                        # 流程启动成功,应返回 fiid,做启动流程的异步配对
                        # 此时,流程 **必须** 提供 fiid 参数!
                        # 流程的返回格式应是 Array or Object 之一，优先按照 Array 判断：
                        #
                        # 1: Array::
                        #
                        #   <RpcId>, <FIID>[, data]
                        #
                        # 2: Object::
                        #
                        #   {
                        #       "id": "the-rpc-id 必须",
                        #       "fiid": "the-flow-instance-id 必须",
                        #       "data": "任意类型JSON数据 可选"
                        #   }
                        if parsed.args:
                            # Array 形式
                            rpc_id = parsed.args[0]
                            fiid = parsed.args[1]
                            try:
                                data = parsed.args[2]
                            except KeyError:
                                data = None
                        elif parsed.kwargs:
                            # Object 形式
                            rpc_id = parsed.kwargs['id']
                            fiid = parsed.kwargs['fiid']
                            data = parsed.kwargs.get('data')
                        else:
                            raise ValueError(
                                'Unknown parameters of _ivr.start_success method: {}'.format(
                                    msg
                                ))
                        try:
                            fut = cls._call_future_map.pop(rpc_id)
                        except KeyError:
                            pass
                        else:
                            fut.set_result({'fiid': fiid, 'data': data})
                    elif parsed.method == '_ivr.start_fail':
                        # 流程启动失败
                        # 此时,流程 **必须** 提供 message 作为错误信息参数!
                        # 流程的返回格式应是 Array or Object 之一，优先按照 Array 判断：
                        #
                        # 1: Array::
                        #
                        #   <RpcId>[, message]
                        #
                        # 2: Object::
                        #
                        #   {
                        #       "id": "the-rpc-id 必须",
                        #       "error": {
                        #           "code": 0, // 错误码 Integer
                        #           "message": "", // 错误描述 String
                        #           "data": "" // 错误数据 任意类型JSON数据
                        #       }
                        #   }
                        if parsed.args:
                            # Array 形式
                            rpc_id = parsed.args[0]
                            err_dict = parsed.args[1]
                        elif parsed.kwargs:
                            # Object 形式
                            rpc_id = parsed.kwargs['id']
                            err_dict = parsed.kwargs['error']
                        else:
                            raise ValueError(
                                'Unknown parameters of _ivr.start_fail method: {}'.format(
                                    msg
                                ))
                        err_obj = error.get_error(**err_dict)
                        try:
                            fut = cls._call_future_map.pop(rpc_id)
                        except KeyError:
                            pass
                        else:
                            fut.set_exception(err_obj)
                    else:
                        # 普通的流程 -> WS 请求
                        logger.debug('>>> request[%s] %s(*args=%s, **kwargs=%s)',
                                     parsed.id, parsed.method, parsed.args, parsed.kwargs)
                        try:
                            mth = RpcContainer[parsed.method]
                        except KeyError:
                            result = jsonrpc.MethodNotFoundError()
                        else:
                            if inspect.iscoroutinefunction(mth):
                                result = await mth(*parsed.args, **parsed.kwargs)
                                result = recursive_jsonable(result)
                            elif callable(mth):
                                func = partial(
                                    mth, *parsed.args, **parsed.kwargs)
                                result = await asyncio.get_event_loop().run_in_executor(None, func)
                                result = recursive_jsonable(result)
                            else:
                                raise RuntimeError(
                                    'Can not execute {!r} as a RPC'.format(mth))
                except Exception as err:
                    logger.error('[FLOW => WS] method=%s : 其它错误: %r : id=%s',
                                 parsed.method, err, parsed.id)
                    send_data = jsonrpc.Error.from_exception(err, parsed.id)
                    cls.send_response(
                        head.src_unit_id, head.src_unit_client_id, parsed.id, send_data.to_json())
                else:
                    if parsed.id:
                        if isinstance(result, Exception):
                            send_data = jsonrpc.Error.from_exception(
                                result, parsed.id)
                        else:
                            send_data = jsonrpc.Response(parsed.id, result)
                        cls.send_response(
                            head.src_unit_id, head.src_unit_client_id,
                            parsed.id, send_data.to_json())
                finally:
                    dur = time() - begin_time
                    logger.debug(
                        '<<< request[%s] %s(). Duration=%s', parsed.id, parsed.method, dur)
            # JSON-RPC Response
            elif isinstance(parsed, (jsonrpc.Response, jsonrpc.Error)):
                try:
                    fut = cls._call_future_map.pop(parsed.id)
                except KeyError:
                    pass
                else:
                    if isinstance(parsed, jsonrpc.Response):
                        fut.set_result(parsed.result)
                    elif isinstance(parsed, jsonrpc.Error):
                        fut.set_exception(parsed)
        except Exception:
            logger.exception(
                'Un-caught exception in async JSON-RPC processing')
            raise

    @classmethod
    def _client_on_data(cls, head, data):
        loop = cls._loop
        assert loop is not None
        msg = to_str(data)
        loop.call_soon_threadsafe(loop.create_task, cls._handle(head, msg))

    @classmethod
    def _client_on_flow_resp(cls, head, project_id, invoke_id, params):
        pass

    @classmethod
    def _client_on_flow_timeout(cls, head, project_id, invoke_id):
        pass

    @classmethod
    def _client_on_flow_error(cls, head, project_id, invoke_id, error_code):
        pass

    @classmethod
    def send_response(cls, unit_id, process_id, id_, msg, expires=5.):  # pylint:disable=invalid-name,too-many-arguments
        """
        Send Response or Error message text
        """
        project_id = id_.split('.')[0]
        cls._client.notify(unit_id, process_id, project_id,
                           id_, 0, float(expires), msg)

    @classmethod
    async def run_flow(  # pylint:disable=invalid-name,too-many-arguments
            cls,
            server_unit_id: int, server_process_index: int, project_id: str, flow_id: str,
            params: Optional[Dict[Text, Any]] = None, timeout: Optional[Union[int, float]] = None
    ) -> Dict[Text, Any]:
        """启动流程
        """
        # pylint:disable=too-many-locals
        loop = cls._loop
        assert loop is not None
        sender = cls._client.unit_id, cls._client.client_id
        if not params:
            params = {}
        if not isinstance(params, dict):
            raise TypeError(
                'Expected type of `params` is `dict`, but actual is `{}`'.format(
                    type(params)
                ))
        try:
            timeout = float(timeout)
        except TypeError:
            timeout = cls.DEFAULT_FLOW_TIMEOUT
        if timeout < 0:
            timeout = cls.DEFAULT_FLOW_TIMEOUT
        rpc_id = uuid1().hex
        var_list = [sender, rpc_id, params]
        fut = cls._call_future_map[rpc_id] = loop.create_future()
        try:
            cls._client.launch_flow(
                server_unit_id, server_process_index, project_id, flow_id, 1, 0, var_list)
        except Exception:
            fut.cancel()  # 有异常，取消异步等待
            raise
        else:
            # 无异常，异步等待返回结果!
            result = await asyncio.wait_for(fut, timeout, loop=loop)
            return result
        finally:
            try:
                del cls._call_future_map[rpc_id]
            except KeyError:
                pass

    @classmethod
    async def opt_flow(  # pylint:disable=invalid-name,too-many-arguments
            cls,
            fiid: Text, method: Text = '',
            params: Optional[Dict[Text, Any]] = None,
            timeout: Optional[float] = None,
            expires: Optional[float] = None
    ) -> Optional[Union[List[Any], Dict[Text, Any]]]:
        """操作正在运行的流程
        """
        # pylint:disable=too-many-locals
        parts = fiid.split('')
        fp_id_parts = parts[0].split('.')
        project_flow_parts = parts[1].split('.')
        unit_id = int(fp_id_parts[0])
        process_index = int(fp_id_parts[1])
        project_id = project_flow_parts[0]
        # 格式： [sender, request]
        sender = cls._client.unit_id, cls._client.client_id
        # params
        if not params:
            params = {}
        if not isinstance(params, dict):
            raise TypeError(
                'Expected type of `params` is `dict`, but actual type is `{}`'.format(type(params)
                                                                                      ))
        # timeout
        try:
            timeout = float(timeout)
        except TypeError:
            timeout = cls.DEFAULT_FLOW_TIMEOUT
        if timeout < 0:
            timeout = cls.DEFAULT_FLOW_TIMEOUT
        # expires
        try:
            expires = float(expires)
        except TypeError:
            expires = cls.DEFAULT_FLOW_EXPIRES
        if expires < 0:
            expires = cls.DEFAULT_FLOW_EXPIRES
        # request
        request = {'method': str(method), 'params': params}
        data = sender, request
        # 先启动异步等待返回
        id_ = request['id'] = uuid1().hex
        fut = cls._call_future_map[id_] = cls._loop.create_future()
        # 发送
        try:
            cls._client.notify(unit_id, process_index,
                               project_id, fiid, 0, expires, json_dumps(data))
        except Exception:
            # 有异常，取消异步等待
            fut.cancel()
            raise
        else:
            # 无异常，异步等待
            return await asyncio.wait_for(fut, timeout)
        finally:
            try:
                del cls._call_future_map[id_]
            except KeyError:
                pass


def recursive_jsonable(obj, encoding='utf-8'):
    """
    递归转换可 JSON 序列化的简单数据类型变量
    """
    # pylint:disable=too-many-return-statements
    if obj is None:
        return obj
    if isinstance(obj, (int, float, bool)):
        return obj
    if isinstance(obj, str):
        return obj
    if isinstance(obj, bytes):
        return obj.decode(encoding)
    if isinstance(obj, datetime.datetime):
        return obj.strftime('%Y-%m-%d %H:%M:%S')
    if isinstance(obj, datetime.date):
        return obj.strftime('%Y-%m-%d')
    if isinstance(obj, datetime.time):
        return obj.strftime('%H:%M:%S')
    if isinstance(obj, (tuple, list)):
        return [recursive_jsonable(i) for i in obj]
    if isinstance(obj, dict):
        return dict((recursive_jsonable(k), recursive_jsonable(v)) for (k, v) in obj.items())
    return str(obj)
