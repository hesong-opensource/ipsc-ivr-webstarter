# -*- coding: utf-8 -*-

"""
IPSC remote Exceptions definitions
"""

try:
    import json
except ImportError:
    import ujson as json


class IpscError(Exception):
    """
    远端的`IPSC`服务器`IVR`运行其异常的基础类
    """

    def __init__(self, code: int = 0, message: str = '', data=None):
        self._code = code
        self._message = message
        self._data = data
        super().__init__(message)

    @property
    def code(self):
        return self._code

    @property
    def message(self):
        return self._message

    @property
    def data(self):
        return self._data

    @classmethod
    def from_json(cls, data: str):
        params = json.loads(data)
        if isinstance(params, dict):
            return cls(**params)
        if isinstance(params, list):
            return cls(*params)
        raise ValueError("array or object JSON data type expected.")

    @classmethod
    def from_dict(cls, d):  # pylint:disable=invalid-name
        return cls(**d)

    def jsonify(self):
        return {
            'code': self.code,
            'message': self.message,
            'data': self.data,
        }

    def __repr__(self):
        return '<{} object at {!r}. code={!r}, message={!r}, data={!r}>'.format(
            self.__class__.__qualname__, hex(id(self)),
            self._code, self._message, self._data
        )

    def __str__(self):
        return '{}({!r}, {!r}, {!r})'.format(
            self.__class__.__name__, self._code, self._message, self._data
        )


class CallFailError(IpscError):
    """
    IPSC 呼叫失败错误
    """
    __err_code__ = 0
    __err_desc__ = ''

    def __init__(self, cause: int, data):
        self._cause = cause
        super().__init__(self.__err_code__, self.__err_desc__, data)

    @property
    def cause(self):
        return self._cause


class ChannelBusy(CallFailError):
    __err_code__ = 1
    __err_desc__ = '无空闲通道'


class GlobalChannelBusyError(CallFailError):
    __err_code__ = 2
    __err_desc__ = '通道全忙'


class UserBusyError(CallFailError):
    __err_code__ = 3
    __err_desc__ = '用户忙'


class LineBusyError(CallFailError):
    __err_code__ = 4
    __err_desc__ = '线路忙'


class EmptyNumberError(CallFailError):
    __err_code__ = 5
    __err_desc__ = '空号'


class NoDialToneError(CallFailError):
    __err_code__ = 6
    __err_desc__ = '无拨号音'


class DialTimeoutError(CallFailError):
    __err_code__ = 7
    __err_desc__ = '拨号超时'


class DeviceError(CallFailError):
    __err_code__ = 8
    __err_desc__ = '设备错误'


class CallerHangupError(CallFailError):
    __err_code__ = 9
    __err_desc__ = '主叫挂机'


class CallDenied(CallFailError):
    __err_code__ = 10
    __err_desc__ = '呼叫禁止'


class NoRingToneError(CallFailError):
    __err_code__ = 11
    __err_desc__ = '无回铃音'


class CallTerminatedError(CallFailError):
    __err_code__ = 12
    __err_desc__ = '呼叫终止'


class CalledHangupError(CallFailError):
    __err_code__ = 13
    __err_desc__ = '被叫挂机'


class CallsConflictError(CallFailError):
    __err_code__ = 14
    __err_desc__ = '呼叫同抢'


class OtherCallError(CallFailError):
    __err_code__ = 100
    __err_desc__ = '其它呼叫错误'


_CALL_FAIL_MAP = {c.__err_code__: c for c in (
    ChannelBusy, GlobalChannelBusyError, UserBusyError, LineBusyError, EmptyNumberError, NoDialToneError,
    DialTimeoutError, DeviceError, CallerHangupError, CallDenied, NoRingToneError, CallTerminatedError,
    CalledHangupError, CallsConflictError, OtherCallError
)}


def get_error(code, message, data):
    call_fail_cls = _CALL_FAIL_MAP.get(code)
    if call_fail_cls:
        if isinstance(data, dict):
            cause = data.get('cause', 0)
            if cause is None:
                cause = 0
            else:
                cause = int(cause)
        elif isinstance(data, (tuple, list)):
            cause = int(data[0])
        else:
            cause = 0
        return call_fail_cls(cause, data)
    return IpscError(code, message, data)
