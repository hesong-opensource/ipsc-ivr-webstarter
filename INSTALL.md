# IPSC IVR Web Starter

## 目录结构

建议将这个程序，安装在 `/opt/hesong/ipsc-webstarter` 目录。
如果单服务器上要启动多个 `webstarter` 连接 `IPSC`，可以运行多个程序，每个程序使用不同的的配置文件。

在 `/opt/hesong/ipsc-webstarter` 目录中:

- 新建名为`config`的子目录，用于存放配置文件
- 新建名为`logs`的子目录，用于存放日志文件

```sh
sudo mkdir -p /opt/hesong/ipsc-webstarter/config /opt/hesong/ipsc-webstarter/logs
```

其树形结构形如：

```sh
.
├── config
├── env
│   ├── bin
│   ├── include
│   ├── lib
│   ├── lib64 -> lib
│   └── share
└── logs
```

| Directory | Description                     |
| --------- | ------------------------------- |
| `config`  | 配置文件目录                    |
| `env`     | [venv][] 目录，在后面的步骤建立 |
| `logs`    | 日志文件目录                    |

## 准备 Python 环境

### 安装 Python

这是一个使用[Python][]编写的程序，它应该可以兼容的[Python][]版本有：

- `3.5`
- `3.6`
- `3.7`
- 以及比 `3.7` 更高 `3.*` 版本

所以，我们需要在操作系统中安装[Python][]，以下小节记录了如何在我们的常用操作系统下进行安装。

- CentOS 6/7

  本程序建议采用的 [Python][] `3.5.3` 及以上版本，但是 `CentOS 6` 的官方 [yum][] 源未提供该软件包，所以我们需要从源代码编译和安装 [Python][]。

  以下做简单的介绍，具体可参考 [Python][] 和 `CentOS` 官方文档:

  1. 首先使用 [yum][] 安装编译 [Python][] 所需的工具和库文件、头文件:

     ```sh
     sudo yum groupinstall "development tools"
     sudo yum install zlib-devel bzip2-devel xz-devel ncurses-devel readline-devel openssl-devel sqlite-devel
     ```

  1. 然后下载并安装[Python][]`3.*`的最新稳定版本(版本号大于等于`3.5.3`，此处以[python][]`3.6.5`为例):

     ```sh
     cd ~
     wget https://www.python.org/ftp/python/3.6.5/Python-3.6.5.tar.xz
     tar -xf Python-3.6.5.tar.xz
     cd Python-3.6.5
     ./configure
     make
     sudo make altinstall
     ```

  安装完毕后，验证一下，执行:

  ```sh
  $ python3.6 --version
  Python 3.6.5
  ```

  然后执行下面这个命令，确保 [pip][] 可以工作:

  ```sh
  sudo -H python3.6 -m ensurepip
  ```

- Ubuntu 1604/1804

  Ubuntu 1604/1804 在默认安装已经分别带有 [Python][] `3.5`/`3.6`，不必单独安装。
  如遇特殊定制或者非默认安装的 Ubuntu 1604/1804 版本，没有安装 [python][] `3`，可以使用[apt][]进行安装。

  除此之外，我们还需要安装 [Python][] `3` 的其它几个附加功能。一起执行这些安装任务：

  ```sh
  sudo apt install python3 python3-setuptools python3-pip python3-venv
  ```

### Python 虚拟目录

**强烈建议**在[Python][]虚拟环境([venv][])下，运行这个程序。

在`/opt/hesong/ipsc-webstarter`目录中，建立[venv][]目录`env`，并“激活”这个环境:

```sh
cd /opt/hesong/ipsc-webstarter
sudo -H python3.6 -m venv env
```

### 程序包安装方法

在这个[venv][]环境中安装本服务程序包。有多种方式安装：

1. 通过[git][]安装:

   ```sh
   sudo -H env/bin/pip install git+https://bitbucket.org/hesong-opensource/ipsc-ivr-webstarter.git
   ```

   上面命令通过[git][]安装了`master`分支对应的版本。
   如果要指定某个[git][]分支或者标签，可以在`url`后加上加上`@<branch|tag|commit>`，例如要安装`develop`分支对应的版本：

   ```sh
   sudo -H env/bin/pip install git+https://bitbucket.org/hesong-opensource/ipsc-ivr-webstarter.git@develop
   ```

   用于生产的时候，可安装用于加速的可选依赖包，包括 `uvloop`,`ujson`,`aiodns`,`cchardet`。
   以下命令安装这个程序包的`x.y.z`版本(`git tag`是`x.y.z` **具体版本应以实际情况为准**)，且包含了上述用于加速的可选依赖包:

   ```sh
   sudo -H env/bin/pip install git+https://bitbucket.org/hesong-opensource/ipsc-ivr-webstarter.git@x.y.z#egg=ipsc-ivr-webstarter[uvloop,ujson,aiodns,cchardet]
   ```

   **提示**:

   > - 以上是在线安装，需要访问互联网
   > - 通过[git][]+`ssh`安装的时候，需要`SSH KEY`

1. [wheel][]包在线安装

   安装这个程序包的`x.y.z`版本[wheel][]包(**具体版本应以实际情况为准**)。

   此时，目标[Python][]环境应确保已预先安装 [setuptools][], [pip][], [wheel][]；有相关技术人员准备对应于目标环境的[wheel][]包文件。

   ```sh
   sudo -H env/bin/pip install ipsc_ivr_webstarter-x.y.z-py3-none-any.whl
   ```

   用于生产的时候，可安装用于加速的可选依赖包，包括 `uvloop`,`ujson`,`aiodns`,`cchardet` 等。
   以下命令安装这个程序包的`x.y.z`版本，且包含了所有用于加速的可选依赖包:

   ```sh
   sudo -H env/bin/pip install ipsc_ivr_webstarter-x.y.z-py3-none-any.whl[all]
   ```

   **提示**:

   > - 依赖包仍然是在线安装的，需要访问互联网

1. [wheel][]包离线安装

   离线安装也是通过[wheel][]包进行的。

   此时，目标[Python][]环境应确保已预先安装 [setuptools][], [pip][], [wheel][]；有相关技术人员准备对应于目标环境的[wheel][]包文件，包括该程序自身，以及所有依赖的程序包。

   下面的例子将在`wheels`目录中输出所有(包括全部扩展依赖)[wheel][]包:

   ```sh
   mkdir -p wheels
   pip wheel -w wheels .[all]
   ```

   将它们复制到目标环境，然后通过[pip][]进行安装。
   假定所有（包括依赖包）[wheel][]文件都复制到了目录`/dir/of/wheels`，那么，安装脚本是:

   ```sh
   sudo -H env/bin/pip install ipsc_ivr_webstarter-x.y.z-py3-none-any.whl --no-index --find-links /dir/of/wheels
   ```

   加上`[<extra>]`，同时安装附加的可选依赖包，以下命令将安装*全部*的附加依赖包:

   ```sh
   sudo -H env/bin/pip install ipsc_ivr_webstarter-x.y.z-py3-none-any.whl[all] --no-index --find-links /dir/of/wheels
   ```

如果在线安装很慢，可以尝试使用阿里云或者豆瓣的[pip][]源。以阿里云 [pip][] repository 为例，可以预先修改配置文件中的 mirror：

```bash
env/bin/pip config set global.index-url=https://mirrors.aliyun.com/pypi/simple/
```

或者在安装时指定 mirror:

```bash
sudo -H env/bin/pip install ipsc_ivr_webstarter-x.y.z-py3-none-any.whl --index-url=https://mirrors.aliyun.com/pypi/simple/
```

## 升级安装

如果这个程序包发布了新的版本，可以用与安装时一样的方式，进行升级。唯一的不同就是需要在[pip]的`install`命令之后，加上`--upgrade`参数。如:

```sh
sudo -H env/bin/pip install --upgrade ipsc_ivr_webstarter-2.0-py3-none-any.whl
```

```sh
sudo -H env/bin/pip install --upgrade git+https://bitbucket.org/hesong-opensource/ipsc-ivr-webstarter.git@develop
```

## 程序配置

这个程序配置文件的格式采用[YAML][]，它需要两个配置文件:

- 程序配置文件，通常放在`config/prog.yml`，这是该配置文件的默认路径。
- 日志配置文件，通常放在`config/log.yml`，这是该配置文件的默认路径。

这两个文件的初始内容可以通过命令行输出。

- 输出程序配置文件:

  ```sh
  env/bin/hesong-ipsc-webstarter echo_config_sample prog | sudo tee config/prog.yml
  ```

- 输出日志配置文件:

  ```sh
  env/bin/hesong-ipsc-webstarter echo_config_sample log | sudo tee config/log.yml
  ```

配置项的含义见配置文件注释

## 运行程序

执行以下`shell`命令，查看程序运行帮助信息:

```sh
env/bin/hesong-ipsc-webstarter --help
```

## 后台服务

### init

还没有试过……

### systemd

这个应该简单吧……

### supervisord

如果这个程序需要同时运行多个实例，或者配合我们的其它服务程序使用，推荐使用[supervisord]作为这个程序的后台服务管理工具。

#### 安装 supervisord

参考 <http://supervisord.org/installing.html>

- CentOS 6/7

  CentOS 6/7 的官方 [yum][] 源没有提供 [supervisord][]。
  我们推荐这样的在线安装:

  - CentOS 6

    **警告**:

    > 软件版本老旧，有兼容问题风险

    ```sh
    curl https://raw.githubusercontent.com/pypa/get-pip/master/2.6/get-pip.py | sudo -H python2 -
    sudo -H pip2 install supervisor
    ```

  - CentOS 7

    ```sh
    curl https://bootstrap.pypa.io/get-pip.py | sudo -H python2 -
    sudo -H pip2 install supervisor
    ```

  具体安装说明请参考 <http://supervisord.org/installing.html>

  在`/etc`目录新建[supervisord][]配置目录和配置文件:

  ```sh
  sudo mkdir -p /etc/supervisor/conf.d
  echo_supervisord_conf | sudo tee /etc/supervisord.conf
  sudo ln -s /etc/supervisord.conf /etc/supervisor/supervisord.conf
  ```

  修改刚才生成的配置文件`/etc/supervisor/supervisord.conf`，将最后的`include/fils`修改为:

  ```ini
  [include]
  files=/etc/supervisor/conf.d/*.conf
  ```

- Ubuntu 1604/1804

  使用[apt][]安装:

  ```sh
  sudo apt install supervisor
  ```

### 配置 supervisord

具体配置说明请参考 <http://supervisord.org/configuration.html>

在`/etc/supervisor/conf.d`目录下，为这个程序新建配置文件`hesong-ipsc-webstarter.conf`，其内容如下:

```ini
[program:hesong-ipsc-webstarter]
directory = /opt/hesong/ipsc-webstarter
command = /opt/hesong/ipsc-webstarter/env/bin/hesong-ipsc-webstarter run -c config/prog.yml -l config/log.yml
```

### 控制 supervisord

使用`supervisorctl`命令行工具进行操作，具体参考[supervisord][]官方文档。

### supervisord 自动启动

- CentOS 6

  参考 <http://supervisord.org/running.html#running-supervisord-automatically-on-startup>

  我们采用了 <https://github.com/Supervisor/initscripts/blob/master/redhat-init-equeffelec> 这个脚本，存放在`/etc/init.d/supervisord`，且赋予`root`用户/组执行权限:

  ```sh
  curl https://raw.githubusercontent.com/Supervisor/initscripts/master/redhat-init-equeffelec | sudo tee /etc/init.d/supervisord
  sudo chmod ug+x /etc/init.d/supervisord
  sudo chkconfig --add supervisord
  ```

  这样，CentOS 6 将在启动时自动运行[Supervisord][]

- CentOS 7

  尚未实践

- Ubuntu 1604/1804

  通过[apt][]安装后已经建立好了名为`supervisor`的[systemd][]服务。

---

[python]: https://python.org/
[venv]: https://docs.python.org/3/tutorial/venv.html
[pip]: https://pip.pypa.io/
[wheel]: https://wheel.rtfd.org/
[apt]: https://help.ubuntu.com/lts/serverguide/apt.html
[yum]: https://wikipedia.org/wiki/Yellow_Dog_Updater,_Modified
[git]: https://git-scm.com/
[yaml]: http://yaml.org/
[systemd]: https://www.freedesktop.org/wiki/Software/systemd/
[supervisord]: http://supervisord.org/
